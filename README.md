# tvid

## Problem

You want to deploy a bunch of cheap $150 giant TVs to display dashboards and
status stuff, but you don't want to image the Raspberry Pis separately or
hook up keyboards/mouse.

## Solution

Make a single OS image for all your display-driving SBCs that goes to a
non-unique URL: this app.

This app lets you set all the kiosk/display TVs in your organization to the
same URL.  Each will be cookied with a unique ID that will display on each
display in big letters, with no preconfiguration required.

Log in to the admin panel (`/admin`) and enter the URL target for that
display, and within 60 seconds, that display will bounce to that URL, or any
other time it turns on.

You can reconfigure the target URL at any time, and the next time that
display reboots or reloads (you should be rebooting your displays daily) it
will get redirected to the new target.

# configuration knobs

## environment variables

* set `ADMIN_PSK` to the admin password (for `/admin` url)

## state storage

* writes sqlite database into `/data`, mount that volume somewhere

# todo

* fix CSRF bug
* fix FIXMEs
* fix logging output (that is, make it log anything)
* put git short id into version string
* make sure cookie expiration is correct
* sessions maybe
* configuration in db to support password changes
* perhaps load the target in a fullsize iframe so that the target can be
  changed by the js on server update instead of having to reboot the display
  box

# screenshots

## Display ID screen

(This is what gets displayed on a TV.  It animates to avoid screenburn.)

![screenshot one](/sneak/tvid/raw/branch/master/screenshots/1.png)

## Login Page

![screenshot two](/sneak/tvid/raw/branch/master/screenshots/2.png)

## Admin Panel

![screenshot three](/sneak/tvid/raw/branch/master/screenshots/3.png)

# license

WTFPL

# status

This is not enterprise-production-grade software yet.

It has a CSRF bug (low severity) and it stores your unhashed auth password
in a cookie.  I didn't even check how long the per-device cookie expirations
are, so it might have a big painful bug there too.  I made it to scratch an
itch and I am running it in production; I may improve it further but it
works for my purposes (being able to flash a half-dozen display driver
raspberry pis with the same image) as-is.

Patches and improvements and bug reports are welcome.  Send me an email at
the address below.

# author

sneak &lt;[sneak@sneak.berlin](mailto:sneak@sneak.berlin)&gt;
