from sqlalchemy import Column, Sequence, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base

from .server import SQLBASE

Base = SQLBASE
# YAGNI just set admin pw in an env var for now and dont support changing it
# yet
# class Settings(Base):


class TV(Base):
    __tablename__ = "tvs"
    id = Column(Integer, Sequence("id_seq"), primary_key=True)
    displayid = Column(String(20))
    lastSeen = Column(DateTime)
    target = Column(String(255))
    memo = Column(String(255))
