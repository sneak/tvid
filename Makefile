export DOCKER_HOST := ssh://datavi.be
export PYTHONPATH := $(PWD)
export SQLITE_FILENAME := ./db.sqlite

default: docker

peinstall:
	pipenv install --python $(shell which python3)

develop:
	pipenv run python ./bin/tvidd

clean:
	rm -rf build dist tvid.egg-info

docker:
	docker build -t sneak/tvid --build-arg UBUNTU_MIRROR="http://ubuntu.datavi.be/ubuntu" .
