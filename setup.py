from setuptools import setup, find_packages

# for some reason that console_scripts entrypoint fails
# and i have no idea why so there is a bin in bin/

setup(
    name="tvid",
    version="1.0.0",
    packages=find_packages(),
    license="WTFPL",
    long_description=open("README.md").read(),
    entry_points={"console_scripts": ["tvidd = tvid.tvid:serve"],},
)
