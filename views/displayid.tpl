% rebase('base.tpl', refresh='60')
<div id="main">
    <p>
        <i>Display ID:</i>
    </p>
    <h1>{{id}}</h1>
    <small>(Letters only: I like INDIA, O like
    OSCAR.)<br/>
    Powered by tvid v{{version}}</small>
</div>

<script>
    var x = 0;
    var factor = 1;
    function animate() {
        console.log("animate called");
        var elem = document.getElementById("main");
        console.log(elem);
        console.log(elem.style);
        elem.style.marginTop = x + 'px';
        x = x + factor;
        if(x > 1080) {
            factor = -1;
        }
        if(x < 0) {
            factor = 1;
            x = 0;
        }
    }

    $(document).ready(function() {
        var b = document.getElementsByTagName("BODY")[0]
        b.style.background = '#666666';
        console.log("starting...");
        setInterval(animate, 20);
    });
</script>
