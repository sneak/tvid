% rebase('base.tpl', refresh=None, title='tvid administration')
<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
  <a class="navbar-brand" href="/admin">TVID Admin</a>

    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
          <a class="btn btn-secondary" href="/logout">Log Out</a>
      </li>
    </ul>

</nav>


<div class="container" id="adminpanel">
    {{!base}}
    <p><small>Powered by tvid v{{version}}</small></p>
</div>
