% rebase('base.tpl', refresh=None, title=None)

<div class="container">

<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <div class="fadeIn first">
      <h2>tvid administration</h2>
    </div>

    % if defined('msg') and msg:
        <p>
        <div class="card card-body bg-light">{{msg}}</div>
        </p>
    % end



    <!-- Login Form -->
    <form action="/checklogin" method="post">
      <input type="password" id="password" class="fadeIn third"
      name="password" placeholder="password">
      <input type="submit" class="fadeIn fourth" value="Log In">
    </form>

  </div>
</div>
<p><small>Powered by tvid v{{version}}</small></p>

</div>
