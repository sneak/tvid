% rebase('adminpagebase.tpl', refresh=None, title='tvid administration')
<h1>TVs</h1>

<table class="table table-striped table-hover">
  <thead class="thead-dark">
<tr>
  <th scope="col">Display ID</th>
  <th scope="col">Descriptive Memo</th>
  <th scope="col">Last Seen</th>
  <th scope="col">Target URL</th>
  <th scope="col">Edit</th>
</tr>
</thead>
<tbody>

% for tv in tvs:
    <tr>
      <th scope="row">{{tv.displayid}}</th>
      <td>{{tv.memo or '(none)'}}</td>
      <td>{{tv.lastSeen}}</td>
      <td>{{tv.target}}</td>
      <td><a href="/admin/edit/{{tv.displayid}}" class="btn btn-success btn-sm">Edit</a></td>
    </tr>
% end

 </tbody>
</table>
