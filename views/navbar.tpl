% rebase('base.tpl', refresh=None, title='tvid administration')
<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
  <a class="navbar-brand" href="/admin">TVID Admin</a>

    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
          <a class="btn btn-secondary" href="/logout">Log Out</a>
      </li>
    </ul>

</nav>


<div class="container" id="adminpanel">
    <h1>TVs</h1>

    <table class="table table-striped table-hover">
      <thead class="thead-dark">
    <tr>
      <th scope="col">Display ID</th>
      <th scope="col">Last Seen</th>
      <th scope="col">Target URL</th>
      <th scope="col">Edit</th>
    </tr>
  </thead>
  <tbody>

    % for tv in tvs:
        <tr>
          <th scope="row">{{tv.displayid}}</th>
          <td>{{tv.lastSeen}}</td>
          <td>{{tv.target}}</td>
          <td><a href="/admin/edit/{{tv.displayid}}" class="btn btn-success btn-sm">Edit</a></td>
        </tr>
    % end

     </tbody>
</table>

    <p><small>Powered by tvid v{{version}}</small></p>
</div>
