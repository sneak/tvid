% rebase('adminpagebase.tpl', refresh=None, title='tvid administration')

<h1>Edit {{tv.displayid}}</h1>

<div class="container" style="width: 50%">
    <form action="/admin/edit" method="post">


      <div class="form-group">
        <label for="displayid">Display ID</label>
        <input type="text" class="form-control" id="displayid"
            value="{{tv.displayid}}"
            name="displayid"
            placeholder="{{tv.displayid}}"
            readonly>
      </div>

      <div class="form-group">
        <label for="formmemo">Descriptive Memo</label>
        <input type="text" class="form-control" id="formmemo"
            value="{{tv.memo}}"
            name="formmemo"
            placeholder="description/location"
        >
      </div>

      <div class="form-group">
        <label for="targeturl">Target URL</label>
        <input type="text" class="form-control" id="targeturl" name="target" placeholder="https://example.com">
      </div>
      <button type="submit" class="btn btn-primary">Save</button>
    </form>
</div>
