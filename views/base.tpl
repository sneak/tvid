<!doctype html>
<html lang="en">
    <head>
        <title>{{ get('title','tvid') }}</title>
        <meta charset="utf-8">

        % if defined('refresh'):
            <meta http-equiv="refresh" content="{{ get('refresh',60) }}">
        % end

        % include('htmlheader.tpl')

        <link rel="stylesheet" href="/style.css" type="text/css">

    </head>
    <body>
        {{!base}}
    </body>
</html>
