# focal amd64 as of 2020-03-10
FROM ubuntu@sha256:d050ed7278c16ff627e4a70d7d353f1a2ec74d8a0b66e5a865356d92f5f6d87b

################################################################################
##  Mirror Setup
##  - option to use local mirror to speed build
################################################################################
ARG UBUNTU_MIRROR=http://archive.ubuntu.com/ubuntu
RUN echo "deb $UBUNTU_MIRROR focal main universe restricted multiverse" > \
            /etc/apt/sources.list.new && \
    echo "deb $UBUNTU_MIRROR focal-updates main universe restricted multiverse" >> \
            /etc/apt/sources.list.new && \
    echo "deb $UBUNTU_MIRROR focal-security main universe restricted multiverse" >> \
            /etc/apt/sources.list.new && \
    echo "deb $UBUNTU_MIRROR focal-backports main universe restricted multiverse" >> \
            /etc/apt/sources.list.new && \
    mv /etc/apt/sources.list.new /etc/apt/sources.list


ARG UID=61000
ARG GID=61000

RUN groupadd \
        --system --gid $GID \
        app && \
    useradd \
        --system --gid $GID --uid $UID \
        --no-log-init -m -s /bin/false --home-dir /home/app \
        app

################################################################################
##  Versions
################################################################################
# master as of 2020-03-10
ARG PYENV_COMMIT=df9fa1dc30b6448ef8605e2c2d4dfc2a94d6a35d
ARG PYTHON_VERSION=3.8.1

################################################################################
##  Packages
################################################################################
ARG DEBIAN_FRONTEND=noninteractive
RUN apt update && \
    apt upgrade -y && \
    apt install -y \
        build-essential \
        curl \
        git \
        libbz2-dev \
        libffi-dev \
        liblzma-dev \
        libncurses5-dev \
        libncursesw5-dev \
        libreadline-dev \
        libsqlite3-dev \
        libssl-dev \
        llvm \
        locales \
        locales \
        make \
        python-openssl \
        tk-dev \
        wget \
        xz-utils \
        zlib1g-dev \
    && \
    echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8 && \
    mkdir -p /var/app && \
    chown app:app /var/app


ENV LANG en_US.UTF-8

USER app
WORKDIR /home/app
ENV HOME /home/app

RUN git clone https://github.com/pyenv/pyenv.git $HOME/.pyenv && \
    cd $HOME/.pyenv && \
    git checkout $PYENV_COMMIT

ENV PYENV_ROOT $HOME/.pyenv
ENV PATH $PYENV_ROOT/shims:$PYENV_ROOT/bin:$PATH

################################################################################
##  Python
################################################################################
RUN pyenv install $PYTHON_VERSION && \
    pyenv global $PYTHON_VERSION && \
    pyenv rehash && \
    pip install --upgrade pip && \
    pip install pipenv


################################################################################
##  Install App Deps
################################################################################j
WORKDIR /var/app
COPY ./Pipfile ./Pipfile.lock /var/app/
RUN pipenv install --python $PYENV_ROOT/shims/python

################################################################################
##  Install App
################################################################################
COPY . /var/app

VOLUME /data

ENV PYTHONPATH /var/app
CMD pipenv run python ./bin/tvidd
